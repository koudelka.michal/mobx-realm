# MobxRealm

## What is this?
[Mobx](https://github.com/mobxjs/mobx) is a great library to manage your application state. [Realm](https://realm.io/) is a great database for react native. This package is just a compatibility layer which enables reactivity of realm objects and collections inside mobx stores with minimum effect to memory footprint performance.

## How to use it?

1. When declaring your models, set all schema classes to be descendant of _MobxRealmModel_ Like this:
```javascript
import {MobxRealmModel} from 'mobx-realm';

class Agent extends MobxRealmModel {};

Agent.schema = {
    name: 'Agent',
    primaryKey: 'number',
    properties: {
        number: 'int',
        firstName: 'string',
        lastName: 'string',
    }
}

export default Agent;
```

2. Be sure the property is in store is marked as observable and following in your store:
 ```javascript
	import {observable} from 'mobx';
	import {mobxRealmCollection,mobxRealmObject} from 'mobx-realm';

// Let's assume realmDB is initialized somewhere here

class TopSecret {
	@observable allAgents = mobxRealmCollection(realmDB.objects('Agent');
	@observable bestAgent = mobxRealmObject(realmDB.objectForPrimaryKey(7);
}
```
3. That's actually all. Have fun!

## What's missing

1. Tests! (hopefully will be added soon, feel free to add PR)

