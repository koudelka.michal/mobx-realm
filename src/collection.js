import {observable} from 'mobx';

import mobxRealmObject from './object';

function mobxRealmCollection(realmCollection) {
    const observableArray = observable.array();
    realmCollection.forEach((realmObject) => {
        observableArray.push(mobxRealmObject(realmObject))
    });

    realmCollection.addListener((collection,changes) => {
        for (const insertionIndex of changes.insertions) {
          observableArray.splice(
            insertionIndex,
            0,
            mobxRealmObject(realmCollection[insertionIndex])
          );
        }

        for (const deletionIndex of changes.deletions) {
            observableArray.splice(deletionIndex,1);
        }
    });

    return observableArray;
}

export default mobxRealmCollection;
