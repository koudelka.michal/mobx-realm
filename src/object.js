const proxyHandler = {
    get: (target,property) => {
        return target.__mobxReadProperty(property);
    }
}

function mobxRealmObject(realmObject) {
    if (realmObject.hasOwnProperty('__mobxObject')) return realmObject.__mobxObject;
    return new Proxy(
        realmObject,
        proxyHandler
    );
}

export default mobxRealmObject;
